import java.lang.reflect.Array;
import java.net.PortUnreachableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Task5020 {
    public static void main(String[] args) throws Exception {
        // task 1
        int array[] = { 1, 2, 3, 4, 5 };
        boolean a = task1(array);
        System.out.println(a);
        boolean b = task1("hello");
        System.out.println(b);
        // task2
        System.out.println(task2(array, 6));

        int[] array3 = { 8, 7, 3, 6, 8, 9 };
        task3(array3);
        // task4(array3, 3);
        // task4
        System.out.println();
        int n = task4(array3, 5);
        // int m = task4(array3, 7);
        System.out.println(n);
        // System.out.println(m);
        System.out.println("Task 6");
        Object task6[] = { Float.NaN, 0, 15, false, -22, "html", "develop", 47, null };
        task6(task6);
        // task7
        System.out.println("Task7");
        int task71[] = { 2, 5, 9, 6 };
        int task72[] = { 2, 9, 6 };
        int task73[] = { 2, 5, 9, 6 };
        int task74[] = { 2, 5, 9, 6, 5, 5, 7 };

        int task71R[] = tast7(task71, 5);
        int task72R[] = tast7(task72, 5);
        int task73R[] = tast7(task73, 6);
        int task74R[] = tast7(task74, 5);
        System.out.println("task 71R");
        inMang(task71R);
        System.out.println("task 72R");
        inMang(task72R);
        System.out.println("task 73R");
        inMang(task73R);
        System.out.println("task 74R");
        inMang(task74R);
        // task8
        int[] task8Array = { 1, 2, 3 };
        task8(task8Array);
        // task9
        System.out.println("mang task9");
        int[] task9Arr = task9(6, 0);
        inMang(task9Arr);
        int[] task9Arr2 = task9(4, 11);
        inMang(task9Arr2);
        // task10
        int[] task10Arr = task10(1, 4);
        inMang(task10Arr);
        int[] task10Arr2 = task10(-6, 4);
        inMang(task10Arr2);
    }

    static void inMang(int a[]) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }

    public static boolean task1(Object obj) {
        return obj instanceof int[];

    }

    public static String task2(int a[], int n) {
        if (n < a.length) {
            return a[n] + "";
        } else {
            return null;
        }

    }

    public static void task3(int a[]) {
        Arrays.sort(a);
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }

    }

    public static int task4(int a[], int n) {
        int b = -1;
        for (int i = 0; i < a.length; i++) {
            if (a[i] == n) {
                b = i;
            }

        }
        return b;

    }

    public static int[] task5(int a[], int b[]) {

        int[] result = new int[a.length + b.length];
        int index = 0;
        for (int i = 0; i < a.length; i++) {
            result[index++] = a[i];
        }
        for (int i = 0; i < b.length; i++) {
            result[index++] = b[i];
        }
        return result;
    }

    public static void task6(Object obj[]) {
        for (int i = 0; i < obj.length; i++) {
            if (obj[i] instanceof Integer || obj[i] instanceof String) {
                System.out.println(obj[i]);
            }
        }
    }

    public static int[] tast7(int a[], int n) {

        int count = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] == n) {
                count++;
            }
        }
        int[] result = new int[a.length - count];
        int index = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != n) {
                result[index++] = a[i];
            }
        }
        return result;

    }

    public static void task8(int a[]) {
        Random random = new Random();
        int randomNumber = random.nextInt(a.length);
        int number = a[randomNumber];
        System.out.println("Số ngẫu nhiên trong mảng: " + number);
    }

    public static int[] task9(int x, int y) {
        int a[] = new int[x];
        int index = 0;
        for (int i = 0; i < a.length; i++) {
            a[index++] = y;
        }
        return a;
    }

    public static int[] task10(int x, int y) {
        int a[] = new int[y];
        int index = 0;
        for (int i = 0; i < a.length; i++) {
            a[index++] = x++;
        }
        return a;
    }

}
