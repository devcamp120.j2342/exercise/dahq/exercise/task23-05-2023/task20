public class Task5050 {
    public static void main(String[] args) throws Exception {
        // task1
        System.out.println("Số lần xuất hiện của kí tự là: " + task1("eeabcggtbeeee", "e"));
        // task2
        System.out.println("Task 2: " + task2(" hello "));
        // task3
        System.out.println("Task3: " + task3("This is my house this", "this"));
        // task5
        System.out.println("Task5: " + task5("ABcd", "abcd"));
        System.out.println("Task5: " + task5("ABcd", "abcrd"));
        // task6
        System.out.println("Task6: " + task6("hElLo", 1));
        // task7
        System.out.println("Task7: " + task7("hElLo", 0));
        // task9
        System.out.println("Task9: " + task9(""));
        // task10
        System.out.println("Task10: " + task10("AaBbc"));
        // task4
        System.out.println("Task4: " + task4("Hello welcom", "welcom"));
        // task8
        System.out.println("Task8: " + task8("Hello welcom", "Hello"));

    }

    public static int task1(String s, String n) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.substring(i, i + 1).equals(n)) {
                count++;
            }
        }
        return count;

    }

    public static String task2(String s) {
        return s.trim();
    }

    public static String task3(String s, String sub) {
        String[] string = s.split(" ");
        String result = "";
        for (int i = 0; i < string.length; i++) {
            if (!string[i].equals(sub)) {
                result += string[i] + " ";
            }
        }
        return result;
    }

    public static boolean task5(String s1, String s2) {
        return s1.toUpperCase().equals(s2.toUpperCase());
    }

    public static boolean task6(String s, int n) {
        int character = s.charAt(n);
        return character < 96;
    }

    public static boolean task7(String s, int n) {
        int character = s.charAt(n);
        return character > 96;
    }

    public static boolean task9(String s) {
        return s == "";
    }

    public static String task10(String s) {
        String result = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            result += s.charAt(i);
        }
        return result;
    }

    public static boolean task4(String s, String sub) {
        String[] strings = s.split(" ");

        if (strings[strings.length - 1].equals(sub)) {
            return true;

        }
        return false;
    }

    public static boolean task8(String s, String sub) {
        String[] strings = s.split(" ");

        if (strings[0].equals(sub)) {
            return true;
        }

        return false;
    }

}
