public class Task5010 {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        int tong = sumNumbersV1();
        System.out.println(tong);
        int[] number1 = { 1, 5, 10 };
        int[] number2 = { 1, 2, 3, 5, 7, 9 };
        int tong1 = sumNumbersV2(number1);
        int tong2 = sumNumbersV2(number2);
        System.out.println(tong1);
        System.out.println(tong2);
        printHello(10);
        printHello(9);
    }

    public static int sumNumbersV1() {
        int tong = 0;
        for (int i = 1; i < 101; i++) {
            tong += i;
        }
        return tong;
    }

    public static int sumNumbersV2(int a[]) {
        int tong = 0;
        for (int i = 0; i < a.length; i++) {
            tong += a[i];
        }
        return tong;
    }

    public static void printHello(int number) {
        if (number % 2 == 0) {
            System.out.println(number + " Là số chẳn");
        } else {
            System.out.println(number + " Là số lẻ");
        }
    }

}
