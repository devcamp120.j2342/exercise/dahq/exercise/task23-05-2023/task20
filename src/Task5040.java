import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Task5040 {
    public static void main(String[] args) throws Exception {
        int task1Arr[] = task1(-3, 5);
        System.out.println(Arrays.toString(task1Arr));
        // task3
        int task3Arr[] = { 1, 1, 2, 3, 5, 1, 2, 1 };
        System.out.println("Task 3: " + task3(task3Arr, 1));
        // task4
        int task4Arr[] = { 1, 1, 2, 3, 5, 1, 2, 1 };
        System.out.println("Task4: " + task4(task4Arr));
        // task5
        int task5Arr[] = { 1, 1, 2, 4, 5, 8, 2, 1 };
        task5(task5Arr);
        System.out.println();
        // task6
        int task6Arr1[] = { 1, 2, 4, 5 };
        int task6Arr2[] = { 5, 7, 8, 9, 1, 3, 4, 5 };
        task6(task6Arr1, task6Arr2);
        System.out.println(Arrays.toString(task6(task6Arr1, task6Arr2)));
        // task7
        Integer task7Arr[] = { 1, 2, 3, 1, 2, 3, 4, 4, 6, 5, 2, 9, 4, 8 };
        System.out.println(Arrays.toString(task7(task7Arr)));
        // task8
        int task8Arr1[] = { 1, 2, 3 };
        int task8Arr2[] = { 5, 7, 8, 9, 5 };
        // System.out.println(Arrays.toString(task8(task8Arr1, task8Arr2)));
        // task9
        int task9Arr2[] = { 5, 7, 8, 9, 5 };
        System.out.println(Arrays.toString(task9(task9Arr2)));
        // task10
        int task10Arr[] = { 5, 7, 8, 9 };
        System.out.println(Arrays.toString(task10(task10Arr, 0, 1)));
        // task2
        Integer task2Arr1[] = { 1, 2, 3 };
        Integer task2Arr2[] = { 5, 7, 8, 9, 5 };
        System.out.println(Arrays.toString(task2(task2Arr1, task2Arr2)));

    }

    public static int[] task1(int x, int y) {
        int a[] = new int[y - x + 1];
        int index = 0;
        for (int i = x; i <= y; i++) {
            a[index++] = i;
        }
        return a;
    }

    public static int task3(int a[], int n) {
        int count = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] == n) {
                count++;
            }

        }
        return count;
    }

    public static int task4(int a[]) {
        int tong = 0;
        for (int i = 0; i < a.length; i++) {
            tong += a[i];
        }
        return tong;
    }

    public static void task5(int a[]) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                System.out.print(a[i] + " ");
            }
        }
    }

    public static int[] task6(int a[], int b[]) {
        int max = 0;
        if (a.length > b.length) {
            max = a.length;
        } else {
            max = b.length;
        }
        int result[] = new int[max];
        int bNew[] = new int[max];
        int aNew[] = new int[max];
        int indexa = 0;
        int indexb = 0;
        if (a.length > b.length) {

            for (int i = 0; i < b.length; i++) {
                bNew[indexb++] = b[i];
            }
            for (int i = 0; i < a.length; i++) {
                aNew[i] = a[i];
            }

        } else {
            for (int i = 0; i < a.length; i++) {
                aNew[indexa++] = a[i];
            }
            for (int i = 0; i < b.length; i++) {
                bNew[i] = b[i];
            }

        }

        for (int i = 0; i < max; i++) {
            result[i] = aNew[i] + bNew[i];
        }
        return result;

    }

    public static Integer[] task7(Integer a[]) {
        Set<?> set = new HashSet<>(Arrays.asList(a));
        Integer[] result = set.toArray(new Integer[0]);
        return result;
    }

    public static int[] task8(int a[], int b[]) {
        int len = a.length + b.length;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                if (a[i] == b[j]) {
                    len--;
                }
            }
        }
        System.out.println(len);
        int result[] = new int[len];
        int index = 0;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < a.length; j++) {
                if (b[i] != a[j]) {
                    result[i] = b[i];
                }
            }
        }
        return result;
    }

    public static int[] task9(int a[]) {
        Arrays.sort(a);
        return a;
    }

    public static int[] task10(int a[], int x, int y) {
        int b[] = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            b[i] = a[i];
        }
        b[x] = a[y];
        b[y] = a[x];
        return b;
    }

    // task2 gop 2 mang bor cac phan tu trung lap
    public static Integer[] task2(Integer a[], Integer b[]) {
        Integer c[] = Arrays.copyOf(a, a.length + b.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        Set<?> set = new HashSet<>(Arrays.asList(c));
        Integer[] result = set.toArray(new Integer[0]);
        return result;

    }

}
