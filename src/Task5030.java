import java.util.Arrays;

public class Task5030 {
    public static void main(String[] args) throws Exception {
        System.out.println(task1("happy"));
        System.out.println(task1(1));
        System.out.println(task2("Hello world"));
        // task3
        System.out.println("Task3");
        String[] task3Arr = task3("Xin chao devcamp 120");
        inMang(task3Arr);
        // task4
        System.out.println(task4("Hello woll"));
        // task6
        System.out.println(task6("hi are you"));
        // task7
        task7("Hello!", 5);
        // task8
        task8("xinchaoVietnammmm", 2);
        // task9
        System.out.println("Số lần xuất hiện của chuỗi con : " + task9("The Number of the dog the house  name", "the"));
        // task10
        System.out.println("Task10: " + task10("000022", "333333333333"));
        //
        // task5
        System.out.println("Task5: " + task5("Hello world sss   "));
    }

    public static void inMang(String s[]) {
        for (int i = 0; i < s.length; i++) {
            System.out.println(s[i]);
        }

    }

    public static boolean task1(Object obj) {
        return obj instanceof String;
    }

    public static String task2(String s) {
        String result = "";

        result = s.substring(0, 4);
        return result;
    }

    public static String[] task3(String s) {

        String[] result = s.split(" ");
        return result;

    }

    public static String task4(String s) {
        String[] result = s.split(" ");
        String string = "";
        for (int i = 0; i < result.length - 1; i++) {
            string += result[i] + "-";

        }
        return string + result[result.length - 1];
    }

    public static String task6(String s) {
        String[] string = s.split(" ");
        String result = "";
        for (int i = 0; i < string.length; i++) {
            result += string[i].substring(0, 1).toUpperCase() + string[i].substring(1, string[i].length()) + " ";
        }
        return result;
    }

    public static void task7(String s, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i + 1; j++) {
                System.out.print(s);
            }
            System.out.println();
        }

    }

    public static void task8(String s, int n) {
        int len = s.length();
        if (len % n == 0) {
            String[] result = new String[len / n];
            for (int i = 0; i < len; i += n) {
                result[i / n] = s.substring(i, i + n);

            }
            System.out.println(Arrays.toString(result));

        } else {
            String[] result = new String[(len / n) + 1];
            for (int i = 0; i < len - (len % n); i += n) {
                result[i / n] = s.substring(i, i + n);

            }
            result[len / n] = s.substring(len - (len % n), len);
            System.out.println(Arrays.toString(result));
        }

    }

    public static int task9(String s, String sub) {
        String[] result = s.split(" ");
        System.out.println(Arrays.toString(result));
        int count = 0;
        for (int i = 0; i < result.length; i++) {
            if (result[i].toUpperCase().equals(sub.toUpperCase())) {
                count++;
            }
        }
        System.out.println(result[4]);
        return count;
    }

    public static String task10(String s, String sub) {
        String result = s.substring(0, Math.abs(s.length() - sub.length())) + sub;
        return result;

    }

    public static String task5(String s) {
        return s.replaceAll("\\s+", "");
    }

}
